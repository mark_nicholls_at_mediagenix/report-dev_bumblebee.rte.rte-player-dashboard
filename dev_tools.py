import subprocess
import click
from shutil import copyfile, copy
import glob
import itertools
import os

@click.command()
def test():
    print("======================= run_all_testing ============")
    run_test()

@click.command()
def create():
    run_create()

@click.command()
def publish():
    run_test()
    run_create()
    #test_launch()
    print("======================= Publish =======================")
    branch = subprocess.check_output(["git","branch","--show-current"]).decode("utf-8").rstrip().replace('/','_')
    jos = branch[:branch.find('-',branch.find('-') + 1)]
    print(f"jos={jos}")
    print(f"branch={branch}")
    def get_version(filename: str) -> int:
        return int(list(filename.split('.'))[2])
    publish_root = '//oops/dataroot/DeliveryData/Deployments/BumblebeeReports/RTE/rte_player_dashboard/2021r3'
    app_name = 'rte_player_dashboard'
    print(f'globbing {publish_root}/{app_name}.{jos}.*')
    existing_files = glob.glob(f'{publish_root}/{app_name}.{jos}.*')
    version = max(itertools.chain([0],map(get_version,existing_files))) + 1 
    # do this weirdness to create files that are easy to deploy in bumblebee
    folder = f'{publish_root}/{app_name}.{jos}.{version}'
    os.mkdir(folder)
    filename = f'{publish_root}/{app_name}.{jos}.{version}/{app_name}.{jos}.{version}.exe'
    print(f"copying {app_name}.exe to {filename}".replace('/','\\'))
    copyfile(f"dist/{app_name}.exe",filename)
    copy('report_def/RTE Player Dashboard.xml',folder)
    # copy('install_instructions.rtf',folder)
    print("done")

def run_test():
    print("======================= run_tests =======================")
    poetry_run_cmd = ["poetry","run"]
    print("======================= pytest =======================")
    subprocess.run(
        [*poetry_run_cmd, *["pytest","--cov-report","html","tests/","--cov=rte_player_dashboard" ]], 
        shell=True)
    subprocess.run(["start","htmlcov/index.html"], shell=True)

def run_create():
    print("======================= clean-up =======================")
    subprocess.run(["del","rte_player_dashboard.spec"], shell=True)
    subprocess.run(["rmdir","dist","/s","/q"], shell=True)
    subprocess.run(["rmdir","build","/s","/q"], shell=True)
    print("======================= create exe =======================")
    subprocess.run(
        ["pyinstaller","--onefile","rte_player_dashboard/__main__.py","--name","rte_player_dashboard.exe"], 
        shell=True)

