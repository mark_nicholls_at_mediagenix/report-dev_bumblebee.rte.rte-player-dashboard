from typing import Generator
from xml.etree.ElementTree import Element

from .jsonoutput import JsonOutput
from .app_framework import TimestampGetter
from .won_schema import RawFile
from .xmlhelper import element

def transform(timestamp_getter: TimestampGetter, root_element: Element) -> Generator[JsonOutput,None,None]: 
    return JsonOutput.make(
        RawFile.make(root_element),
        timestamp_getter,
        element(root_element,'exportinformation/timestamp/ESP_TIMEINSTANT'))
