from __future__ import annotations
from dataclasses import dataclass
from datetime import date
from typing import Dict, Any, Generator, List, Optional

import xml.etree.ElementTree as ET
import itertools
from rte_player_dashboard.app_framework import TimestampGetter
from rte_player_dashboard.won_schema import ES_REGIONGROUP, ES_TRANSMISSION, ESP_PLANNINGCATEGORY, RawFile
import rte_player_dashboard.optional as Maybe
from rte_player_dashboard.wonhelper import won_to_datetime, won_to_date

def make_programme_rights_view_model(raw_file: RawFile, originalTxForPublication: ES_TRANSMISSION) -> Optional[tuple[date,Dict[str,Any]]]:
    publication_record_platform = next((
        param.value()
        for param in raw_file.parameters()
        if param.name() == 'publication_record_platform'
    ),None)

    record_maybe = next((
        pub_record
        for tx in originalTxForPublication.publicationTransmissions()
        for pubSystem in Maybe.to_list(tx.publicationSystem_maybe())
        if pubSystem.psName() == publication_record_platform
        for pub_record in Maybe.to_list(tx.firstTxBlock().txb_publicationrecord())
    ), None)

    if record_maybe is None:
        return None
    else:
        record = record_maybe
        parent_series_code_maybe = [
            pseries.p_product_productcode()
            for program in Maybe.to_list(record.record_program_maybe())
            for series in Maybe.to_list(program.p_episode_series())
            for pseries in Maybe.to_list(series.parentSerie())
        ]

        series_code_maybe = [
            series.p_product_productcode()
            for program in Maybe.to_list(record.record_program_maybe())
            for series in Maybe.to_list(program.p_episode_series())
        ]

        produce_code_maybe = [
            program.p_product_productcode()
            for program in Maybe.to_list(record.record_program_maybe())
        ]

        showID = next(itertools.chain(parent_series_code_maybe,series_code_maybe,produce_code_maybe,iter([''])))
        episode = record.record_editableinformation().internalEpisodeNumber() if record.record_editableinformation().internalEpisodeNumber() else '1'
        season_name = next((
            "Season " + (program.p_product_masterseriesyear_maybe() or '1')
            for program in Maybe.to_list(record.record_program_maybe())
        ),"")

        def get_path(cat: ESP_PLANNINGCATEGORY) -> List[str]:
            parent_maybe = cat.parent_maybe()
            if parent_maybe:
                return get_path(parent_maybe) + [cat.name()]
            else:
                return [ cat.name() ]

        all_categories = (
            get_path(cat)
            for cat in originalTxForPublication.overallPlanningCategories()
        )

        sorted_by_head = sorted(all_categories,key=lambda item: item[0])
        subcategories_grouped_by_root : List[tuple[str,set[str]]] = [
            (k, set((x[1] for x in g if len(x) > 1)))
            for k, g in itertools.groupby(sorted_by_head,lambda item: item[0])
        ]

        displayType = next((
            lookup.translation1()
            for program in Maybe.to_list(record.record_program_maybe())
            for mode in Maybe.to_list(program.p_product_productionmode())
            for lookup in mode.popupLookups()
            if lookup.attribute() == "Display Type"
        ),'')

        original_tx_for_catchup_maybe = Maybe.to_list(
            originalTxForPublication.originalTxForCatchUp_maybe())

        original_channel = next((
            lookup.translation1()
            for original_tx in original_tx_for_catchup_maybe
            for lookup in original_tx.tx_channel().popupLookups()
            if lookup.attribute() == "Channel translation for MPX"
        ),'')

        distributor = ", ".join((
            firm.f_name()
            for program in Maybe.to_list(record.record_program_maybe())
            for firm in program.p_program_productionfirms()
        ))

        rights_maybe = Maybe.to_list(
            originalTxForPublication.tx_broadcastright_maybe())

        partners = ", ".join(set(itertools.chain(
            (
                firm.f_name()
                for right in rights_maybe
                for contract in Maybe.to_list(right.contract_maybe())
                for cost in contract.costDefinitions()
                for firm in cost.involvedParty()
            ),
            (
                firm.f_name()
                for right in rights_maybe
                for contract in Maybe.to_list(right.productInContract_maybe())
                for cost in contract.costDefinitions()
                for firm in cost.involvedParty()
            ),
            (
                firm.f_name()
                for right in rights_maybe
                for contract in Maybe.to_list(right.broadcastRightGroup_maybe())
                for cost in contract.costDefinitions()
                for firm in cost.involvedParty()
            )
        )))

        certs = [
            cert
            for program in Maybe.to_list(record.record_program_maybe())
            for asset in Maybe.to_list(program.preferredMediaAsset_maybe())
            for cert in asset.regionalcertifications()
        ]

        ratings = ", ".join((
            prog_cert.usercode()
            for cert in certs
            for prog_cert in Maybe.to_list(cert.rpc_certification_maybe())
        ))

        infos = [
            info
            for cert in certs
            for info in cert.rpc_complianceInformations()
        ]

        def compliance_slate_calculation():
            if "true" in (
                boolean
                for info in infos
                if info.field().name() == "Competition Closed"
                for boolean in Maybe.to_list(info.boolean())
            ):
                yield "CompetitionClosed"
            if "true" in (
                info.boolean()
                for info in infos
                if info.field().name() == "Comment Lines Closed"
            ):
                yield "CommentLinesClosed"
            if "true" in (
                program.hasProductPlacement()
                for program in Maybe.to_list(record.record_program_maybe())
            ):
                yield "ProductPlacement"

        # no delimeter!
        compliance_slate = "".join(compliance_slate_calculation())

        def warning_slate_calculation():
            if "true" in (
                info.boolean()
                for info in infos
                if info.field().name() == "Strobing"
            ):
                yield "FlashingLights"
            if "true" in (
                info.boolean()
                for info in infos
                if info.field().name() == "Viewer Discretion Advised"
            ):
                yield "ViewerDiscretionAdvised"
            if "true" in (
                info.boolean()
                for info in infos
                if info.field().name() == "Mature Audience Warning"
            ):
                yield "MatureAudience"

        # no delimeter!
        warning_slate = "".join(warning_slate_calculation())

        def won_to_iso(e: ET.Element) -> str:
            dt = won_to_datetime(e)
            return "{:04}-{:02}-{:02}T{:02}:{:02}:{:02}".format(dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)

        def won_to_iso_date(e: ET.Element) -> str:
            dt = won_to_date(e)
            return "{:04}-{:02}-{:02}".format(dt.year,dt.month,dt.day)

        tx_date = next((
            won_to_iso(tx.startTimePointInUtc().element)
            for tx in original_tx_for_catchup_maybe
        ),"")

        drm = "Downloadable" in (
            period_type.identificationdisplaystring()
            for period_wrapper in originalTxForPublication.attributePeriodWrappers()
            for period in Maybe.to_list(period_wrapper.attributePeriod_maybe())
            for period_type in Maybe.to_list(period.attributePeriodType_maybe())
        )

        license_window_description_maybe = [
            desc
            for right in rights_maybe
            for window in Maybe.to_list(right.singleLicenseWindow_maybe())
            for desc in Maybe.to_list(window.licenseWindowDescription_maybe())
        ]

        all_license_geos = [
            regionGroup
            for desc in license_window_description_maybe
            for regionGroup in desc.regionGroups()
        ]

        def min_license_geos():
            def geo_path(geo: ES_REGIONGROUP) -> List[ES_REGIONGROUP]:
                parent_maybe = geo.parent_maybe()
                if parent_maybe:
                    return [geo] + geo_path(parent_maybe)
                else:
                    return [geo]

            return ", ".join([
                geo.usercode_maybe() or ''
                for geo in all_license_geos
                # only take geos who's ancestor isnt already listed
                if not([
                    1
                    for ancestor in geo_path(geo)[1:]
                    for geo2 in all_license_geos
                    if geo2.oid() == ancestor.oid()
                ])
            ])

        is_on_demand_catchup = next((
            True
            for right in Maybe.to_list(originalTxForPublication.tx_broadcastright_maybe())
            for type in Maybe.to_list(right.broadcastRightDescription().broadcastRightType())
            if type.name() == 'On Demand Catch Up'
        ),False)

        license_start = next((
            won_to_iso_date(start.element) + "T00:00:00"
            for tx in original_tx_for_catchup_maybe
            for right in Maybe.to_list(tx.tx_broadcastright_maybe())
            for window in Maybe.to_list(right.singleLicenseWindow_maybe())
            for start in Maybe.to_list(window.calculatedStartDate())
        ),None) if is_on_demand_catchup else won_to_iso(
            originalTxForPublication.startTimePointInUtc().element) 

        license_end = next((
            won_to_iso_date(start.element) + "T23:59:59"
            for tx in original_tx_for_catchup_maybe
            for right in Maybe.to_list(tx.tx_broadcastright_maybe())
            for window in Maybe.to_list(right.singleLicenseWindow_maybe())
            for start in Maybe.to_list(window.calculatedEndDate())
        ),None) if is_on_demand_catchup else won_to_iso(
            originalTxForPublication.publicationEndTimeInUTC().element) 

        # license_start = next((
        #     won_to_iso(tx.tx_starttimepoint().element)
        #     for tx in originalTxForPublication_maybe
        # ),"")

        # license_end = next((
        #     won_to_iso(tx.tx_publicationendtimepoint().element)
        #     for tx in originalTxForPublication_maybe
        # ),"")

        modified = raw_file.timestamp().fullgmt()

        available_resolution = ", ".join((
            lookup.translation1()
            for desc in license_window_description_maybe
            for definition in desc.definitionGroups()
            for lookup in definition.popupLookups()
            if lookup.attribute() == "Available resolution"
        ))

        device_groups = [
            dev.name()
            for desc in license_window_description_maybe
            for dev in desc.deviceGroups()
        ]

        def platforms() -> List[dict[str,list[dict[str,str]]]]:
            publication_txs = list(originalTxForPublication.publicationTransmissions())

            def publications_txs_by_parameter(p_name: str) -> List[ES_TRANSMISSION]:
                return [
                    tx
                    for tx in publication_txs
                    for system in Maybe.to_list(tx. publicationSystem_maybe())
                    for param in filter(lambda p: p.name() == p_name ,raw_file.parameters())
                    if param.value() == system.psName()
                ]

            desktop_platform_txs = publications_txs_by_parameter("desktop_platform")
            mobile_platform_txs = publications_txs_by_parameter("mobile_platform")
            chromecast_platform_txs = publications_txs_by_parameter("chromecast_platform")
            connectedtv_platform_txs = publications_txs_by_parameter("connectedtv_platform")

            def extract_start_time_default(txs: List[ES_TRANSMISSION]) -> str:
                return next(map(lambda tx: won_to_iso(tx.startTimePointInUtc().element) ,desktop_platform_txs),"")

            def extract_end_time_default(txs: List[ES_TRANSMISSION]) -> str:
                return next(map(lambda tx: won_to_iso(tx.publicationEndTimeInUTC().element) ,desktop_platform_txs),"")

            desktop = {
                "Desktop": [{
                    "AvailableStart": extract_start_time_default(desktop_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(desktop_platform_txs)
                }]
            } if 'Desktop' in device_groups or 'All' in device_groups else None

            mobile = {
                "Mobile": [{
                    "AvailableStart": extract_start_time_default(mobile_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(mobile_platform_txs)
                }]
            } if 'Mobile' in device_groups or 'All' in device_groups else None

            chromecast = {
                "Chromecast": [{
                    "AvailableStart": extract_start_time_default(chromecast_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(chromecast_platform_txs)
                }]
            } if 'Chromecast' in device_groups or 'All' in device_groups else None

            connected_tv = {
                "ConnectedTV": [{
                    "AvailableStart": extract_start_time_default(connectedtv_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(connectedtv_platform_txs)
                }]
            } if 'ConnectedTV' in device_groups or 'All' in device_groups else None

            virgin_platform_txs = publications_txs_by_parameter("virgin_platform")
            skystb_platform_txs = publications_txs_by_parameter("skystb_platform")
            skygo_platform_txs = publications_txs_by_parameter("skygo_platform")

            virgin = {
                "Virgin": [{
                    "AvailableStart": extract_start_time_default(virgin_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(virgin_platform_txs)
                }]
            } if virgin_platform_txs else None

            sky_go = {
                "SkyGo": [{
                    "AvailableStart": extract_start_time_default(skygo_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(skygo_platform_txs)
                }]
            } if skygo_platform_txs else None

            sky_stb = {
                "SkySTB": [{
                    "AvailableStart": extract_start_time_default(skystb_platform_txs)
                },{
                    "AvailableEnd": extract_end_time_default(skystb_platform_txs)
                }]
            } if skystb_platform_txs else None

            return [
                platform
                for platform in [
                    desktop,
                    mobile,
                    chromecast,
                    connected_tv,
                    virgin,
                    sky_go,
                    sky_stb
                ] 
                if platform
            ] 

        file_date = won_to_date(originalTxForPublication.tx_date().element)

        isl = next((
            True
            for program in Maybe.to_list(record.record_program_maybe())
            for version in Maybe.to_list(program.p_product_productversion())
            if version.name() == "ISL"
        ) , False)

        json = {
            "ShowName": Maybe.map(lambda product: product.p_product_title(),record.record_program_maybe()) or '',
            "DisplayName": record.record_editableinformation().record_translatedtitle(),
            "ShowID": showID,
            "ShowNameMid": record.record_editableinformation().record_title()[0:50],
            "ShowNameShort": record.record_editableinformation().record_title()[0:19],
            "Season": Maybe.bind(lambda product:product.p_product_masterseriesyear_maybe(),record.record_program_maybe()) or '1',
            "Year": record.record_editableinformation().record_productionyear(),
            "Episode": episode,
            "EpisodeName": record.record_editableinformation().record_episodetitle(),
            "BRN": Maybe.map(lambda product: product.p_product_productcode(), record.record_program_maybe()) or '',
            "SeasonName": season_name,
            "PlayerGenres" : [
                { "Value": [
                    {
                        "parentGenre": parent
                    },
                    {
                        "subGenres": [
                            {
                                "Value": sub
                            }
                            for sub in sorted(subs)
                        ]
                    }
                ]}
                for parent, subs in subcategories_grouped_by_root
            ],
            "DisplayType": displayType,
            "OriginalChannel": original_channel,
            "Highlight": True if next(record.highlightedTxs(),None) else False,
            "ISL": isl,
            "AD": record.record_editableinformation().record_isaudiodescribed() == "true",
            "Distributor": distributor,
            "Partner": partners,
            "Rating": ratings,
            "ComplianceSlate": compliance_slate,
            "WarningSlate": warning_slate,
            "TXDate": tx_date,
            "DRM": drm,
            "SeriesDescription": record.record_editableinformation().record_shortdescriptionprintedpressforseries(),
            "EpisodeDescription": record.record_editableinformation().record_shortdescriptionprintedpress(),
            "LicenceGeos": min_license_geos(),
            "LicenceStart": license_start,
            "LicenceEnd": license_end,
            "Modified": modified,
            "RightsOffers": {
                "TotalOffers": 1,
                "Offers":{
                    "0": [{
                        "isVOD": True,
                        "AvailableResolution": available_resolution,
                        "DeviceRestrictions": None,
                        "DomainsEmbeddable": None,
                        # same as license geos
                        "AvailableGeos": min_license_geos(),
                        "Platforms": platforms()
                    }]
                }
            },
        }
        return (file_date,json)

@dataclass
class JsonOutput:
    """ Holds report description """
    filename: str
    json: Dict[str, Any]

    @staticmethod
    def make(
            raw_file: RawFile,
            timestamp_getter: TimestampGetter,
            timestamp: ET.Element) -> Generator[JsonOutput,None,None]:
        publication_records : Generator[tuple[date,Dict[str,Any]],None,None] = (
            result
            for record in raw_file.transmissions()
            for result in Maybe.to_list(make_programme_rights_view_model(raw_file,record))
        )

        sorted_publication_records = sorted(            
            publication_records,
            key=lambda tuple: tuple[0]
        )

        grouped_publication_records = (
            (k, list(record[1] for record in g))
            for k, g in itertools.groupby(sorted_publication_records,lambda item: item[0])
        )

        timestamp_datetime = timestamp_getter(timestamp)

        return (
            JsonOutput("PlayerDashboardXML-TXDate-{:04}-{:02}-{:02}T{:02}_{:02}_{:02}.json".format(
                dt.year,
                dt.month,
                dt.day,
                timestamp_datetime.hour,
                timestamp_datetime.minute,
                timestamp_datetime.second),{
                "result" : records
            })
            for dt,records in grouped_publication_records
        )
