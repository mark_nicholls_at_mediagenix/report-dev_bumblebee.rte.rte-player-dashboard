import logging
from .wonhelper import won_to_datetime
import sys
from typing import Callable
import datetime
import xml.etree.ElementTree as ET


# in order to facilitate testing, we inject a component to evaluare the timestamp
# (else it changes everytime we refresh the test data)
TimestampGetter = Callable[[ET.Element],datetime.datetime]

def make_production_timestamp() -> TimestampGetter:
    # this is the default production mechanism, that simply takes the timestamp element
    # and turns it into a datetime 
    return won_to_datetime

def make_testing_timestamp(test_timestamp: datetime.datetime) -> TimestampGetter:
    # this the testing version, where the XML timestamp is ignored in favour of a
    # specified one
    return lambda _: test_timestamp 

def config_logging(logging_level: str) -> logging.Logger:
    logging.basicConfig(level=logging_level)
    log = logging.getLogger(__name__)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging_level)
    log.addHandler(handler)
    return log

def handle_exception(exc_type, exc_value, exc_traceback):
    """ global exception handler, logs errors, and then maps them to an exit code """
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    log = logging.getLogger(__name__)
    log.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    log.info('')
    log.info('command line args')
    for i,arg in enumerate(sys.argv[1:]):
        log.info('arg[%s] = "%s"',i,arg)
    exitcode = 0
    try:
        raise exc_value
    except FileExistsError:
        exitcode = 2
    except FileNotFoundError:
        exitcode = 3
    except PermissionError:
        exitcode = 4
    except MemoryError:
        exitcode = 5
    except OSError:
        exitcode = 6
    except OverflowError:
        exitcode = 7
    except UnicodeEncodeError:
        exitcode = 8
    except UnicodeDecodeError:
        exitcode = 9
    except Exception:
        exitcode = 1
    sys.exit(exitcode)
