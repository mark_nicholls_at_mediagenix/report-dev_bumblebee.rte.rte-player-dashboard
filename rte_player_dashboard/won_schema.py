from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Generator, Optional

from xml.etree.ElementTree import Element

from .xmlhelper import attribute, attribute_maybe, element, element_maybe  # , attribute_maybe
import rte_player_dashboard.optional as Maybe

@dataclass
class RawFile:
    element: Element

    @staticmethod
    def make(element: Element) -> RawFile:
        return RawFile(element)

    def transmissions(this) -> Generator[ES_TRANSMISSION,Any,Any]:
        return (
            ES_TRANSMISSION(element)
            for element in this.element.findall('ES_TRANSMISSION')
        )

    def timestamp(this) -> ESP_TIMEINSTANT:
        return ESP_TIMEINSTANT(element(this.element,"exportinformation/timestamp/ESP_TIMEINSTANT"))

    def parameters(this) -> Generator[parameter,Any,Any]:
        return (
            parameter(element)
            for element in this.element.findall('parameters/parameter')
        ) 

@dataclass
class parameter:
    element: Element

    def name(this) -> str:
        return attribute(this.element,'name')

    def value(this) -> str:
        return attribute(this.element,'value')

@dataclass
class ES_PUBLICATIONRECORD:
    element: Element

    def record_program_maybe(this) -> Optional[ES_PRODUCT]:
        return Maybe.map(ES_PRODUCT,element_maybe(this.element,'record_program/ES_PRODUCT'))

    def record_editableinformation(this) -> ES_PROGRAMGUIDERECORDINFORMATION:
        return ES_PROGRAMGUIDERECORDINFORMATION(element(this.element,"record_editableinformation/ES_PROGRAMGUIDERECORDINFORMATION"))

    def pg_id_txblock_maybe(this) -> Optional[ES_TXBLOCK]:
        return Maybe.map(ES_TXBLOCK,element_maybe(this.element,'pg_id_txblock/ES_TXBLOCK'))

    def highlightedTxs(this) -> Generator[ES_HIGHLIGHTEDTRANSMISSION,None,None]:
        return (
            ES_HIGHLIGHTEDTRANSMISSION(element)
            for element in this.element.findall('highlightedTxs/ES_HIGHLIGHTEDTRANSMISSION')
        )

@dataclass
class ES_TXBLOCK:
    element: Element

    def txb_publicationrecord(this) -> Optional[ES_PUBLICATIONRECORD]:
        return Maybe.map(ES_PUBLICATIONRECORD,element_maybe(this.element,'txb_publicationrecord/ES_PUBLICATIONRECORD'))

    def txb_tx(this) -> ES_TRANSMISSION:
        return ES_TRANSMISSION(element(this.element,"txb_tx/ES_TRANSMISSION"))

@dataclass
class ES_TRANSMISSION:
    element: Element

    def tx_date(this) -> ESP_DATE:
        return ESP_DATE(element(this.element,'tx_date/ESP_DATE'))

    def firstTxBlock(this) -> ES_TXBLOCK:
        return ES_TXBLOCK(element(this.element,'firstTxBlock/ES_TXBLOCK'))

    def overallPlanningCategories(this) -> Generator[ESP_PLANNINGCATEGORY,None,None]:
        return (
            ESP_PLANNINGCATEGORY(element)
            for element in this.element.findall('overallPlanningCategories/ESP_PLANNINGCATEGORY')
        )

    def originalTxForPublication_maybe(this) -> Optional[ES_TRANSMISSION]:
        return Maybe.map(ES_TRANSMISSION,element_maybe(this.element,'originalTxForPublication/ES_TRANSMISSION'))

    def originalTxForCatchUp_maybe(this) -> Optional[ES_TRANSMISSION]:
        return Maybe.map(ES_TRANSMISSION,element_maybe(this.element,'originalTxForCatchUp/ES_TRANSMISSION'))

    def tx_channel(this) -> ESP_CHANNEL:
        return ESP_CHANNEL(element(this.element,"tx_channel/ESP_CHANNEL"))    

    def tx_broadcastright_maybe(this) -> Optional[ES_CM2BROADCASTRIGHT]:
        return Maybe.map(ES_CM2BROADCASTRIGHT,element_maybe(this.element,'tx_broadcastright/ES_CM2BROADCASTRIGHT'))

    # def tx_starttimepoint(this) -> ESP_TIMEINSTANT:
    #     return ESP_TIMEINSTANT(element(this.element,"tx_starttimepoint/ESP_TIMEINSTANT"))

    def startTimePointInUtc(this) -> ESP_TIMEINSTANT:
        return ESP_TIMEINSTANT(element(this.element,"startTimePointInUtc/ESP_TIMEINSTANT"))

    def attributePeriodWrappers(this) -> Generator[ES_ATTRIBUTEPERIODWRAPPER,None,None]:
        return (
            ES_ATTRIBUTEPERIODWRAPPER(element)
            for element in this.element.findall('attributePeriodWrappers/ES_ATTRIBUTEPERIODWRAPPER')
        )

    # def tx_publicationendtimepoint(this) -> ESP_TIMEINSTANT:
    #     return ESP_TIMEINSTANT(element(this.element,"tx_publicationendtimepoint/ESP_TIMEINSTANT"))

    def publicationEndTimeInUTC(this) -> ESP_TIMEINSTANT:
        return ESP_TIMEINSTANT(element(this.element,"publicationEndTimeInUTC/ESP_TIMEINSTANT"))

    def publicationTransmissions(this) -> Generator[ES_TRANSMISSION,None,None]:
        return (
            ES_TRANSMISSION(element)
            for element in this.element.findall('publicationTransmissions/ES_TRANSMISSION')
        )

    def publicationSystem_maybe(this) -> Optional[ES_PGPUBLICATIONSYSTEM]:
        return Maybe.map(ES_PGPUBLICATIONSYSTEM,element_maybe(this.element,'publicationSystem/ES_PGPUBLICATIONSYSTEM'))

@dataclass
class ES_PGPUBLICATIONSYSTEM:
    element: Element

    def psName(this) -> str:
        return attribute(this.element,'psName')

@dataclass
class ES_ATTRIBUTEPERIODWRAPPER:
    element: Element

    def attributePeriod_maybe(this) -> Optional[ES_ATTRIBUTEPERIOD]:
        return Maybe.map(ES_ATTRIBUTEPERIOD,element_maybe(this.element,'attributePeriod/ES_ATTRIBUTEPERIOD'))

@dataclass
class ES_ATTRIBUTEPERIOD:
    element: Element

    def attributePeriodType_maybe(this) -> Optional[ESP_ENUMERATION]:
        return Maybe.map(ESP_ENUMERATION,element_maybe(this.element,'attributePeriodType/ESP_ENUMERATION'))

@dataclass
class ESP_ENUMERATION:
    element: Element

    def identificationdisplaystring(this) -> str:
        return attribute(this.element,'identificationdisplaystring')

@dataclass
class ESP_PRODUCTVERSION:
    element: Element

    def name(this) -> str:
        return attribute(this.element,'name')

@dataclass
class ES_PRODUCT:
    element: Element

    def p_product_title(this) -> str:
        return attribute(this.element,'p_product_title')

    def p_product_productversion(this) -> Optional[ESP_PRODUCTVERSION]:
        return Maybe.map(ESP_PRODUCTVERSION,element_maybe(this.element,'p_product_productversion/ESP_PRODUCTVERSION'))

    def p_episode_series(this) -> Optional[ES_PRODUCT]:
        return Maybe.map(ES_PRODUCT,element_maybe(this.element,'p_episode_series/ES_PRODUCT'))

    def parentSerie(this) -> Optional[ES_PRODUCT]:
        return Maybe.map(ES_PRODUCT,element_maybe(this.element,'parentSerie/ES_PRODUCT'))

    def p_product_productcode(this) -> str:
        return attribute(this.element,'p_product_productcode')

    def p_product_masterseriesyear_maybe(this) -> Optional[str]:
        return attribute_maybe(this.element,'p_product_masterseriesyear')

    def p_product_productionmode(this) -> Optional[ESP_PRODUCTIONMODE]:
        return Maybe.map(ESP_PRODUCTIONMODE,element_maybe(this.element,'p_product_productionmode/ESP_PRODUCTIONMODE'))

    def p_program_productionfirms(this) -> Generator[ES_FIRM,None,None]:
        return (
            ES_FIRM(element)
            for element in this.element.findall('p_program_productionfirms/ES_FIRM')
        )

    def preferredMediaAsset_maybe(this) -> Optional[ES_MM2MEDIAASSET]:
        return Maybe.map(ES_MM2MEDIAASSET,element_maybe(this.element,'preferredMediaAsset/ES_MM2MEDIAASSET'))

    def hasProductPlacement(this) -> str:
        return attribute(this.element,'hasProductPlacement')

@dataclass
class ES_MM2MEDIAASSET:
    element: Element

    def regionalcertifications(this) -> Generator[ES_WONREGIONALPROGRAMCERTIFICATION,None,None]:
        return (
            ES_WONREGIONALPROGRAMCERTIFICATION(element)
            for element in this.element.findall('regionalcertifications/ES_WONREGIONALPROGRAMCERTIFICATION')
        )

@dataclass
class ES_WONREGIONALPROGRAMCERTIFICATION:
    element: Element

    def rpc_certification_maybe(this) -> Optional[ESP_PROGRAMCERTIFICATION]:
        return Maybe.map(ESP_PROGRAMCERTIFICATION,element_maybe(this.element,'rpc_certification/ESP_PROGRAMCERTIFICATION'))

    def rpc_complianceInformations(this) -> Generator[ES_ComplianceInformation,None,None]:
        return (
            ES_ComplianceInformation(element)
            for element in this.element.findall('rpc_complianceInformations/ES_ComplianceInformation')
        )

@dataclass
class ES_ComplianceInformation:
    element: Element

    def field(this) -> ESP_REGULATORCOMPLIANCECONFIGURATIONENTRYFIELD:
        return ESP_REGULATORCOMPLIANCECONFIGURATIONENTRYFIELD(element(this.element,"field/ESP_REGULATORCOMPLIANCECONFIGURATIONENTRYFIELD"))    

    def boolean(this) -> Optional[str]:
        return attribute_maybe(this.element,'boolean')

@dataclass
class ESP_REGULATORCOMPLIANCECONFIGURATIONENTRYFIELD:
    element: Element

    def name(this) -> str:
        return attribute(this.element,'name')

@dataclass
class ESP_PROGRAMCERTIFICATION:
    element: Element

    def usercode(this) -> str:
        return attribute(this.element,'usercode')

@dataclass
class ES_FIRM:
    element: Element

    def f_name(this) -> str:
        return attribute(this.element,'f_name')

@dataclass
class ESP_CHANNEL:
    element: Element

    def popupLookups(this) -> Generator[POPUPLOOKUP,None,None]:
        return (
            POPUPLOOKUP(element)
            for element in this.element.findall('popupLookups/POPUPLOOKUP')
        )

@dataclass
class POPUPLOOKUP:
    element: Element

    def attribute(this) -> str:
        return attribute(this.element,'attribute')

    def translation1(this) -> str:
        return attribute(this.element,'translation1')

@dataclass
class ESP_PRODUCTIONMODE:
    element: Element

    def popupLookups(this) -> Generator[POPUPLOOKUP,None,None]:
        return (
            POPUPLOOKUP(element)
            for element in this.element.findall('popupLookups/POPUPLOOKUP')
        )

@dataclass
class ESP_PLANNINGCATEGORY:
    element: Element

    def parent_maybe(this) -> Optional[ESP_PLANNINGCATEGORY]:
        return Maybe.map(ESP_PLANNINGCATEGORY,element_maybe(this.element,'parent/ESP_PLANNINGCATEGORY'))

    def name(this) -> str:
        return attribute(this.element,'name')

@dataclass
class ESP_TIMEINSTANT:
    element: Element

    def fullgmt(this) -> str:
        return attribute(this.element,'fullgmt')

@dataclass
class ES_HIGHLIGHTEDTRANSMISSION:
    element: Element


@dataclass
class ESP_2BROADCASTRIGHTTYPE:
    element: Element

    def name(this) -> Optional[str]:
        return attribute_maybe(this.element,'name')

@dataclass
class ES_CM2BROADCASTRIGHTDESCRIPTION:
    element: Element

    def broadcastRightType(this) -> Optional[ESP_2BROADCASTRIGHTTYPE]:
        return Maybe.map(ESP_2BROADCASTRIGHTTYPE,element_maybe(this.element,'broadcastRightType/ESP_2BROADCASTRIGHTTYPE'))

@dataclass
class ES_CM2BROADCASTRIGHT:
    element: Element

    def contract_maybe(this) -> Optional[ES_CMContract]:
        return Maybe.map(ES_CMContract,element_maybe(this.element,'contract/ES_CMContract'))

    def productInContract_maybe(this) -> Optional[ES_CMProductInContract]:
        return Maybe.map(ES_CMProductInContract,element_maybe(this.element,'productInContract/ES_CMProductInContract'))

    def broadcastRightGroup_maybe(this) -> Optional[ES_BROADCASTRIGHTGROUP]:
        return Maybe.map(ES_BROADCASTRIGHTGROUP,element_maybe(this.element,'broadcastRightGroup/ES_BROADCASTRIGHTGROUP'))

    def singleLicenseWindow_maybe(this) -> Optional[ES_CM2LicenseWindowDescription]:
        return Maybe.map(ES_CM2LicenseWindowDescription,element_maybe(this.element,'singleLicenseWindow/ES_CM2LicenseWindowDescription'))

    def broadcastRightDescription(this) -> ES_CM2BROADCASTRIGHTDESCRIPTION:
        return ES_CM2BROADCASTRIGHTDESCRIPTION(element(this.element,'broadcastRightDescription/ES_CM2BROADCASTRIGHTDESCRIPTION'))

@dataclass
class ES_CM2LicenseWindowDescription:
    element: Element

    def licenseWindowDescription_maybe(this) -> Optional[ES_CM2LicenseWindowDescription]:
        return Maybe.map(ES_CM2LicenseWindowDescription,element_maybe(this.element,'licenseWindowDescription/ES_CM2LicenseWindowDescription'))

    def regionGroups(this) -> Generator[ES_REGIONGROUP,None,None]:
        return (
            ES_REGIONGROUP(element)
            for element in this.element.findall('regionGroups/ES_REGIONGROUP')
        )

    def definitionGroups(this) -> Generator[ES_DEFINITIONGROUP,None,None]:
        return (
            ES_DEFINITIONGROUP(element)
            for element in this.element.findall('definitionGroups/ES_DEFINITIONGROUP')
        )

    def deviceGroups(this) -> Generator[ES_DEVICEGROUP,None,None]:
        return (
            ES_DEVICEGROUP(element)
            for element in this.element.findall('deviceGroups/ES_DEVICEGROUP')
        )

    def calculatedStartDate(this) -> Optional[ESP_DATE]:
        return Maybe.map(ESP_DATE,element_maybe(this.element,'calculatedStartDate/ESP_DATE'))

    def calculatedEndDate(this) -> Optional[ESP_DATE]:
        return Maybe.map(ESP_DATE,element_maybe(this.element,'calculatedEndDate/ESP_DATE'))

@dataclass
class ESP_DATE:
    element: Element

@dataclass
class ES_DEVICEGROUP:
    element: Element

    def name(this) -> str:
        return attribute(this.element,'name')

@dataclass
class ES_DEFINITIONGROUP:
    element: Element

    def popupLookups(this) -> Generator[POPUPLOOKUP,None,None]:
        return (
            POPUPLOOKUP(element)
            for element in this.element.findall('popupLookups/POPUPLOOKUP')
        )

@dataclass
class ES_REGIONGROUP:
    element: Element

    def parent_maybe(this) -> Optional[ES_REGIONGROUP]:
        return Maybe.map(ES_REGIONGROUP,element_maybe(this.element,'parent/ES_REGIONGROUP'))

    def oid(this) -> str:
        return attribute(this.element,'oid')

    def usercode_maybe(this) -> Optional[str]:
        return attribute_maybe(this.element,'usercode')

@dataclass
class ES_CMProductInContract:
    element: Element

    def costDefinitions(this) -> Generator[ES_CM2CostDefinition,None,None]:
        return (
            ES_CM2CostDefinition(element)
            for element in this.element.findall('costDefinitions/ES_CM2CostDefinition')
        )

@dataclass
class ES_BROADCASTRIGHTGROUP:
    element: Element

    def costDefinitions(this) -> Generator[ES_CM2CostDefinition,None,None]:
        return (
            ES_CM2CostDefinition(element)
            for element in this.element.findall('costDefinitions/ES_CM2CostDefinition')
        )

@dataclass
class ES_CMContract:
    element: Element

    def costDefinitions(this) -> Generator[ES_CM2CostDefinition,None,None]:
        return (
            ES_CM2CostDefinition(element)
            for element in this.element.findall('costDefinitions/ES_CM2CostDefinition')
        )

@dataclass
class ES_CM2CostDefinition:
    element: Element

    def involvedParty(this) -> Generator[ES_FIRM,None,None]:
        return (
            ES_FIRM(element)
            for element in this.element.findall('involvedParty/ES_FIRM')
        )

@dataclass
class ES_PROGRAMGUIDERECORDINFORMATION:
    element: Element

    def record_shortdescriptionprintedpress(this) -> Optional[str]:
        return attribute_maybe(this.element,'record_shortdescriptionprintedpress')

    def record_shortdescriptionprintedpressforseries(this) -> Optional[str]:
        return attribute_maybe(this.element,'record_shortdescriptionprintedpressforseries')

    def record_isaudiodescribed(this) -> str:
        return attribute(this.element,'record_isaudiodescribed')

    def record_episodetitle(this) -> str:
        return attribute_maybe(this.element,'record_episodetitle') or ''

    def record_translatedtitle(this) -> str:
        return attribute_maybe(this.element,'record_translatedtitle') or ''

    def record_title(this) -> str:
        return attribute_maybe(this.element,'record_title') or ''

    def record_productionyear(this) -> str:
        return attribute_maybe(this.element,'record_productionyear') or ''

    def internalEpisodeNumber(this) -> str:
        return attribute_maybe(this.element,'internalEpisodeNumber') or ''
