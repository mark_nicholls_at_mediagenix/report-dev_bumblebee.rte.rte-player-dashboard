from xml.etree.ElementTree import Element
import datetime
from .xmlhelper import attribute, attribute_maybe

def won_to_datetime(element: Element) -> datetime.datetime:  
    # maps standard won datetime elements into datatime
    # note will allow milliseconds attribute to be missing
    year = int(attribute(element,'year'))
    month = int(attribute(element,'month'))
    day = int(attribute(element,'day'))
    hours = int(attribute(element,'hours'))
    minutes = int(attribute(element,'minutes'))
    seconds = int(attribute(element,'seconds'))
    microseconds = int(attribute_maybe(element,'milliseconds') or '0') * 1000      
    return datetime.datetime(
        year,
        month,
        day,
        hours,
        minutes,
        seconds,
        microseconds
    )

def won_to_timedelta(element: Element) -> datetime.timedelta:  
    # maps standard won datetime elements into datatime
    # note will allow milliseconds attribute to be missing
    return datetime.timedelta(
        hours = int(attribute(element,'tvdayhours')),
        minutes = int(attribute(element,'minutes')),
        seconds = int(attribute(element,'seconds')),
        milliseconds=int(attribute_maybe(element,'milliseconds') or '0')
    )

def won_to_date(element: Element) -> datetime.date:  
    # maps standard won datetime elements into date
    return datetime.date(
        int(attribute(element,'year')),
        int(attribute(element,'month')),
        int(attribute(element,'day'))
    )

def timedelta_to_hhmmss(delta: datetime.timedelta) -> str:
    hours, remainder = divmod(delta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return '{:02}:{:02}:{:02}'.format(int(hours), int(minutes), int(seconds))
