""" standard functional optional functions """
from typing import Callable, List, Optional, TypeVar

T = TypeVar('T')
U = TypeVar('U')

def map(f: Callable[[T], U], optional: Optional[T]) -> Optional[U] :
    """ map (functor), should be used as optional.map """
    if optional is None:
        return None
    # actually returns a U, but for python everything is optional
    return f(optional)

def bind(f: Callable[[T], Optional[U]], optional: Optional[T]) -> Optional[U] :
    """ bind (monad), should be used as optional.bind """
    if optional is None:
        return None
    return f(optional)

def to_list(optional: Optional[T]) -> List[T]:
    if optional is None:
        return []
    return [optional]
