import os
from subprocess import call

import argparse
import json
import defusedxml.ElementTree as DET

from .transform import transform
from .app_framework import make_production_timestamp

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input',
        type=str, 
        help='input raw xml filename')
    parser.add_argument(
        'destination',
        type=str, 
        help='output destination folder')
    parser.add_argument(
        '--open',
        help='open output in default application',
        action='store_true')
    # note: invalid args DON'T throw exceptions (obviously), they just exit (nastily)
    parsed_args = parser.parse_args()
    root_element = DET.parse(
        parsed_args.input,
        forbid_dtd = True,
        forbid_entities = True,
        forbid_external = True).getroot()
    for result in transform(make_production_timestamp(), root_element):
        output_filename = os.path.join(parsed_args.destination, result.filename)
        with open(output_filename, 'w', encoding='utf8') as outfile:
            json.dump(result.json, outfile,indent=4, sort_keys=False, ensure_ascii=False)
        if parsed_args.open:
            call([
                "explorer.exe",
                output_filename.replace('/','\\') ],
                stdin = None,
                stdout = None,
                stderr = None,
                shell = False)
    return 0
