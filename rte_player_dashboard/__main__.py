import sys
import rte_player_dashboard.main

# pragma: no cover
if __name__ == "__main__": 
    exit_code = rte_player_dashboard.main.main()
    sys.exit(exit_code)
