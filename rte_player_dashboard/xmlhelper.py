from typing import Optional
import xml.etree.ElementTree as ET
import rte_player_dashboard.optional as Maybe

def attribute(element: ET.Element, attribute_name: str) -> str:
    # typesafe attribute get, if you know it should be there, else exception
    value = element.get(attribute_name)
    if (value is None):
        raise Exception(f'@{attribute_name} not found as child of {element.tag}')
    else:
        return value

def element(element: ET.Element, element_path: str) -> ET.Element:
    # typesafe element find, if you know it should be there, else exception
    child = element.find(element_path)
    if (child is None):
        raise Exception(f'{element_path} not found as child of {element.tag}')
    else:
        return child

def attribute_maybe(element_maybe: Optional[ET.Element], attribute_name: str) -> Optional[str]:
    # convenient/consistent way to do a get an optional attribute
    return Maybe.map(lambda e: e.get(attribute_name),element_maybe)

def element_maybe(element_maybe: Optional[ET.Element], element_path: str) -> Optional[ET.Element]:
    # convenient/consistent way to do a find on an optional element
    return Maybe.map(lambda e: e.find(element_path),element_maybe)
