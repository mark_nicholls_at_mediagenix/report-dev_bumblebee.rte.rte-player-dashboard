from rte_player_dashboard import __version__
from tests.test_helper import do_json_test

def test_version():
    assert __version__ == '0.1.0'

def test_everythings_rosie():
    do_json_test('tests/test_everythings_rosie')

def test_sdrop_dead_weird():
    do_json_test('tests/drop_dead_weird')

def test_rtecs_410():
    do_json_test('tests/RTECS-410')
