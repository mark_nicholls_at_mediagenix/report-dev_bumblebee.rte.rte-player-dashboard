from rte_player_dashboard.app_framework import make_testing_timestamp
from rte_player_dashboard.transform import transform
import os.path
import datetime
import json
import os
import jsondiff
import xml.etree.ElementTree as ET

def do_json_test(folder: str):
    root_element: ET.Element = ET.parse(os.path.join(folder,'input.xml')).getroot()
    for result in transform(make_testing_timestamp(datetime.datetime(2021,6,24,15,0,0,0)), root_element):
        filename = os.path.join(folder,result.filename)
        try:
            with open(filename, 'r', encoding='utf-8') as expected_file:
                expected = json.load(expected_file)
            with open(filename, 'w', encoding='utf-8') as outfile:
                json.dump(result.json, outfile, indent=4, sort_keys=False, ensure_ascii=False)
            diffs = jsondiff.diff(expected,result.json)
            assert not(bool(diffs)) 
        except Exception as e:
            with open(filename, 'w', encoding='utf-8') as outfile:
                json.dump(result.json, outfile, indent=4, sort_keys=False, ensure_ascii=False)
            raise Exception('error opening expected file') from e
